﻿using System;

namespace ConsoleApp18
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] initial = new int[] { 7, 2, 11, 3, 1 };
            int[] final;
            final = new int[2 * initial.Length - 1];
            for(int i=0;i<initial.Length;i++)
            {
                final[2 * i] = initial[i];
            }
            for(int i=1;i<final.Length;i=i+2)
            {
                final[i] = final[i - 1] + final[i + 1];
            }
            string mesaj = "";
            for (int i = 0; i < final.Length; i++)
            {
                mesaj = mesaj + "" + final[i];
            }
            Console.WriteLine("");
        }
    }
}
